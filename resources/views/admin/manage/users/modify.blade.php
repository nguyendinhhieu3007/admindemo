@extends('layouts.layout')

@section('style')

@endsection

@section('css')

@endsection

@section('page-breadcrumb')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                {{--<h4 class="page-title">User</h4>--}}
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item">List User</li>
                            <li class="breadcrumb-item active" aria-current="page">Modify User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('container')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Create New User</h4>
                        <form class="form pt-3" action="{{ route('action-create-user') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>User Name</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon11"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" name="username" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon11">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon22"><i class="ti-email"></i></span>
                                    </div>
                                    <input type="text" name="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon22">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon33"><i class="ti-lock"></i></span>
                                    </div>
                                    <input type="text" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon33">
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label>Confirm Password</label>--}}
                                {{--<div class="input-group mb-3">--}}
                                    {{--<div class="input-group-prepend">--}}
                                        {{--<span class="input-group-text" id="basic-addon4"><i class="ti-lock"></i></span>--}}
                                    {{--</div>--}}
                                    {{--<input type="text" class="form-control" placeholder="Confirm Password" aria-label="Password" aria-describedby="basic-addon4">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="custom-control custom-checkbox mr-sm-2">--}}
                                    {{--<input type="checkbox" class="custom-control-input" id="checkbox10" value="check">--}}
                                    {{--<label class="custom-control-label" for="checkbox10">Remember Me</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <button type="submit" class="btn btn-dark">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection

@section('js')
    <script src="">
        $(document).ready(function () {
            // $('#zero_config').DataTable();
        });
    </script>
@endsection