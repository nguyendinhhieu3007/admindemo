@extends('layouts.layout')

@section('style')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('css')

@endsection

@section('page-breadcrumb')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                {{--<h4 class="page-title">User</h4>--}}
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">List Role</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('container')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- basic table -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <div style="display: flex;margin-bottom: 10px">
                                <div class=" col-md-6">
                                    <h4 class="card-title">List Role</h4>
                                </div>
                                <div class="col-md-6 d-flex justify-content-end" style="display:flex">
                                    <button type="button" class="btn waves-effect waves-light btn-outline-primary" onclick="window.location='{{ route('create-role') }}'"><i class="fa fa-plus"></i> Create</button>

                                    <button type="button" class="btn waves-effect waves-light btn-outline-danger "><i class="fa fa-trash"></i> Delete</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th>Name</th>
                                    <th>Detail</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listRole as $key => $value)
                                    @if($value->perId == null)
                                        {{ $value->perName = 'Dưới đáy xã hội' }}
                                    @endif
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <th>{{ $value->roleName }}</th>
                                        <th>{{ $value->perName }}</th>
                                        <th><button type="button" class="btn waves-effect waves-light btn-outline-info "><i class=" fas fa-pencil-alt"></i> Edit</button></th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endsection

@section('js')
    <script src="">
        $(document).ready(function () {

        });
    </script>
@endsection