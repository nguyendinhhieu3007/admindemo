<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\Manage\Users\AdminUserController;
use App\Http\Controllers\Admin\Manage\Permissions\AdminPermissionsController;
use Illuminate\Support\Facades\Route;

Route::match(['get', 'post'], '/login', [LoginController::class, 'login'])->name('admin.login');
Route::middleware('auth:admin')->group(function (){
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');

    Route::get('/list-user', [AdminUserController::class, 'index'])->name('list-user');
    Route::get('/create-user', [AdminUserController::class, 'formCreateUser'])->name('create-user');
    Route::post('/action-create-user', [AdminUserController::class, 'actionCreateUser'])->name('action-create-user');

    Route::get('/list-permission', [AdminPermissionsController::class, 'listPermissions'])->name('list-permission');
    Route::get('/list-role', [AdminPermissionsController::class, 'listRoles'])->name('list-role');
    Route::get('/create-permission', [AdminPermissionsController::class, 'formCreatePermission'])->name('create-permission');
    Route::get('/create-role', [AdminPermissionsController::class, 'formCreateRole'])->name('create-role');
});