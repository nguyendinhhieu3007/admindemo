<?php

namespace App\Http\Controllers\Admin\Manage\Users;

use App\Http\Controllers\Controller;
use App\Services\Admin\AdminUserService;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    private $_adminUserService;

    public function __construct(AdminUserService $adminUserService) {
        $this->_adminUserService = $adminUserService;
    }

    public function index()
    {
        return view('admin/manage/users/list');
    }

    public function formCreateUser()
    {
        return view('admin/manage/users/modify');
    }

    public function actionCreateUser(Request $request){
        $data = $request->all();
        $result = $this->_adminUserService->actionCreateUser($data);
        dd($result);
    }
}
