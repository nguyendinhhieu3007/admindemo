<?php

namespace App\Http\Controllers\Admin\Manage\Permissions;

use App\Http\Controllers\Controller;
use App\Services\Admin\AdminPermissionService;
use Illuminate\Http\Request;

class AdminPermissionsController extends Controller
{
    private $_adminPermissionService;

    public function __construct(AdminPermissionService $adminPermissionService) {
        $this->_adminPermissionService = $adminPermissionService;
    }

    public function listPermissions()
    {
        $listPer = $this->_adminPermissionService->getListPermission();
        return view('admin/manage/permissions/list_permission', compact('listPer'));
    }

    public function listRoles()
    {
        $listRole = $this->_adminPermissionService->getListRole();
        return view('admin/manage/permissions/list_role', compact('listRole'));
    }

    public function formCreatePermission(){

    }

    public function formCreateRole(){

    }
}
