<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/27/2023
 * Time: 3:56 AM
 */

namespace App\Services\Admin;


use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminPermissionService
{
    public function getListPermission(){
        return Permission::all();
    }

    public function getListRole(){
        $listRole = Role::query()
            ->leftJoin('role_has_permissions','roles.id', '=' ,'role_has_permissions.role_id')
            ->leftJoin('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
            ->select('roles.name as roleName','roles.id as roleId')
            ->selectRaw('GROUP_CONCAT(permissions.id) as perId, GROUP_CONCAT(permissions.name) as perName')
            ->groupBy('roles.id')
            ->get();
//        dd($listRole);
        return $listRole;
    }


}